
import math
import struct
import time 
import serial
import re
import struct
import numpy as np

# Basic array to send
# Move to array
# tx_array = [0xA1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB1]
# PID for motor 1 constants template array
# tx_array_1 = [0xA1, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB1]
# PID for motor 2 constants template array
# tx_array_5 = [0xA1, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB1]
# PID for motor 3 constants template array
# tx_array_6 = [0xA1, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB1]
# Read angles array
tx_array_2 = [0xA1, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB1]
# Read position array
tx_array_4 = [0xA1, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB1]
print("  ---------------             --------              -------                -------")
print("/               /           /        /            /       /              /        /")
print("-----------    /           /         /           /    /    /            /         / ")
print("         /    /           /          /          /   / /    /           /          /")
print(" --------    /           /           /         /    /     /           /           /")
print("/           /           /------------/        /         /            /------------/")
print("--------   /           /-------------/       /       /              /-------------/")
print("          /           /              /      /       /              /              /")              
print(" -----   /           /               /     /         /            /               /")
print("/       /           /                /    /           /          /                /")
print("-------/           /                 /   /             /        /                 /")
print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
print("+++++++++++++++++ Welcome to 3ARA soft control ++++++++++++++++++++++++++++++++++++")
print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
print("++++++++++++++++++ Step one: Enter COM port.+++++++++++++++++++++++++++++++++++++++")
print("++++++++++++++++++ Step two: Enter kinematic model.++++++++++++++++++++++++++++++++")
print("++++++++++++++++++ Step three: Enter PID constants.++++++++++++++++++++++++++++++++")

# For asking for the serial COM port, uncomment line 23 and comment line 24.
#comm_port = input("Enter the COM port: ")
comm_port = 'com5'
# Configure the COM port
Comms = serial.Serial()
Comms.baudrate = 9600
Comms.port = comm_port
# Non-blocking mode reading. 
Comms.timeout = 0       

# Shows the configuration
# print(Comms)
# Open the COM port
Comms.open()
# Checks the result 
if Comms.is_open == True:
    print("Port open succesfully!")
else:
    print("An error occurred opening the port\n")

###################### -Private functions- ################################
def waitResponse():
    time.sleep(1)
    if Comms.inWaiting() > 0:
        data = Comms.readlines(Comms.inWaiting())
        print(data)
        Comms.reset_input_buffer()
        data = 0


# Reads the three angles and returns a list with it 
def getAngles():
    i = 0
    buffer_rx = [0, 0, 0]
    while i < 3:
        Comms.write(tx_array_2)
        time.sleep(0.5)
        if Comms.inWaiting() > 0:
            buffer_rx[i] = Comms.readlines(Comms.inWaiting())
            Comms.reset_input_buffer()
            i = i+1
    return buffer_rx        
                     
def getPosition():
    u = 0
    buffer_rx_2 = [0, 0, 0]
    while u < 3:
        Comms.write(tx_array_4)
        time.sleep(0.5)
        if Comms.inWaiting() > 0:
            buffer_rx_2[u] = Comms.readlines(Comms.inWaiting())
            Comms.reset_input_buffer()
            u = u+1
    return buffer_rx_2      

############################## - Global variables- #######################


############################### -Main loop- ##############################
while True:
    Comms.reset_output_buffer()
    in_command = input("Enter a command and press enter: ")
    
    if in_command == "moveto":
        if kin_command == 'forward':
            angle_1 = int(input("Enter angle 1 (Shoulder): "))
            angle_2 = int(input("Enter angle 2 (Arm): "))
            angle_3 = int(input("Enter angle 3 (Forearm): "))
            tx_array = [0xA1, 0x00, angle_1, angle_2, angle_3, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB1]
            Comms.write(tx_array)
            waitResponse()
        elif kin_command == 'inverse':
            x_coord = int(input("Enter x coordinate: "))
            y_coord = int(input("Enter y coordinate: "))
            z_coord = int(input("Enter z coordinate: "))              
            tx_array = [0xA1, 0x00, x_coord, y_coord, z_coord, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB1]
            Comms.write(tx_array)
            waitResponse()

        
    elif in_command == "writepidvar1":
        kp = float(input("Enter Kp value and press enter: "))
        kd = float(input("Enter Kd value and press enter: "))
        ki = float(input("Enter Ki value and press enter: "))
        '''
        # Prints for debug
        print(struct.pack('f', kp))
        print(struct.pack('f', kd))
        print(struct.pack('f', ki))
        '''
        kp_byte_array = struct.pack('f', kp)
        kd_byte_array = struct.pack('f', kd)
        ki_byte_array = struct.pack('f', ki)
        tx_array_1 = [0xA1, 0x01, kp_byte_array[0], kp_byte_array[1], kp_byte_array[2],
                     kp_byte_array[3], kd_byte_array[0],kd_byte_array[1], kd_byte_array[2], kd_byte_array[3],
                      ki_byte_array[0], ki_byte_array[1], ki_byte_array[2],ki_byte_array[3], 0xB1]
                      
        # print(tx_array_1)
        Comms.write(tx_array_1)
        waitResponse()

    elif in_command == "writepidvar2":
        kp_2 = float(input("Enter Kp value and press enter: "))
        kd_2 = float(input("Enter Kd value and press enter: "))
        ki_2 = float(input("Enter Ki value and press enter: "))
        '''
        # Prints for debug
        print(struct.pack('f', kp))
        print(struct.pack('f', kd))
        print(struct.pack('f', ki))
        '''
        kp_byte_array_2 = struct.pack('f', kp_2)
        kd_byte_array_2 = struct.pack('f', kd_2)
        ki_byte_array_2 = struct.pack('f', ki_2)
        tx_array_5 = [0xA1, 0x05, kp_byte_array_2[0], kp_byte_array_2[1], kp_byte_array_2[2],
                     kp_byte_array_2[3], kd_byte_array_2[0],kd_byte_array_2[1], kd_byte_array_2[2], kd_byte_array_2[3],
                      ki_byte_array_2[0], ki_byte_array_2[1], ki_byte_array_2[2],ki_byte_array_2[3], 0xB1]
                      
        # print(tx_array_1)
        Comms.write(tx_array_5)
        waitResponse()

    elif in_command == "writepidvar3":
        kp_3 = float(input("Enter Kp value and press enter: "))
        kd_3 = float(input("Enter Kd value and press enter: "))
        ki_3 = float(input("Enter Ki value and press enter: "))
        '''
        # Prints for debug
        print(struct.pack('f', kp))
        print(struct.pack('f', kd))
        print(struct.pack('f', ki))
        '''
        kp_byte_array_3 = struct.pack('f', kp_3)
        kd_byte_array_3 = struct.pack('f', kd_3)
        ki_byte_array_3 = struct.pack('f', ki_3)
        tx_array_6 = [0xA1, 0x06, kp_byte_array_3[0], kp_byte_array_3[1], kp_byte_array_3[2],
                     kp_byte_array_3[3], kd_byte_array_3[0],kd_byte_array_3[1], kd_byte_array_3[2], kd_byte_array_3[3],
                      ki_byte_array_3[0], ki_byte_array_3[1], ki_byte_array_3[2],ki_byte_array_3[3], 0xB1]
                      
        # print(tx_array_1)
        Comms.write(tx_array_6)
        waitResponse()

    elif in_command == 'getangles':
        angles = getAngles()
        print("Angle 1: ", round(struct.unpack('f', angles[0][0])[0],2),"º")
        print("Angle 2: ", round(struct.unpack('f', angles[1][0])[0],2),"º")
        print("Angle 3: ", round(struct.unpack('f', angles[2][0])[0],2),"º")

    elif in_command == 'getposition':
        position_buffer = getPosition()    
        print("Coordenada X: ", math.floor(struct.unpack('f', position_buffer[0][0])[0]))
        print("Coordenada Y: ", math.floor(struct.unpack('f', position_buffer[1][0])[0]))
        print("Coordenada Z: ", math.floor(struct.unpack('f', position_buffer[2][0])[0]))

    elif in_command == 'kinematic':
        kin_command = input("Choose inverse or forward and press enter: ")
        if kin_command == 'forward':
            tx_array_3 = [0xA1, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB1]
            Comms.write(tx_array_3)
        elif kin_command == 'inverse':
            tx_array_3 = [0xA1, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB1]
            Comms.write(tx_array_3)
        else:
            print("Kinematic model not recognized.\n")

    elif in_command == "routine1":
        print("Routine one starting...\n")
        # Number of routine repetitions
        routine_repetitions = 4
        count = 0
        # Initials angles
        routine_angle_1 = 25
        routine_angle_2 = 45
        routine_angle_3 = 45
        # Final angles
        routine_angle_4 = 90
        routine_angle_5 = 90
        routine_angle_6 = 90
        while(count < routine_repetitions):
            routine_array_1 = [0xA1, 0x00, routine_angle_1, routine_angle_2, routine_angle_3, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB1]
            Comms.write(routine_array_1)
            time.sleep(5)
            routine_array_2 = [0xA1, 0x00, routine_angle_4, routine_angle_5, routine_angle_6, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB1]
            Comms.write(routine_array_2)
            time.sleep(5)
            count += 1
        print("Routine one finished...\n")
        
    elif in_command == 'help':
        print('The list of commands are: ')        
        print('moveto        -> Angles for forward kinematics or coordinates for inverse kinematics')
        print('writepidvar1  -> Change Kp, Kd and Ki values for motor 1.')
        print('writepidvar2  -> Change Kp, Kd and Ki values for motor 2.')
        print('writepidvar3  -> Change Kp, Kd and Ki values for motor 3.')
        print('kinematic     -> Chose "forward" or "inverse" kinematics.')
        print('getangles     -> Shows the current values of the angles. ')
        print('getposition   -> Shows the current coordinates values of the effector.')
        print('routine1      -> Execute routine 1.')
        print('close         -> Close the serial port.\n')    

    elif in_command == "close":
        Comms.close()
        if Comms.is_open == False:
            print("Port close succesfully!")
        break

    else:
        print("Not a command. Try again")      



